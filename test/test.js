const { asset, assert, should } = require('chai')
const { newUser } = require('../index.js')

//describe gives structure to test suite
describe('Test newUser object', () => {
    it('Assert newUser type is object', () => {
        assert.equal(typeof(newUser), 'object');
    })
    it('Assert newUser.email is type string', () => {
        assert.equal(typeof(newUser.email), 'string');
    })
    it('Assert newUser.email is not undefined', () => {
        assert.notEqual(typeof(newUser.email), 'undefined');
    })
    it('Assert newUser.password type is string', () => {
        assert.equal(typeof(newUser.password), 'string');
    })
    it('Assert newUser password length is at least 16 characters', () => {
        assert.isAtLeast((newUser.password.length), 16);
    })
    it('Assert newUser first name type is a string', () => {
        assert.equal(typeof(newUser.firstName), 'string');
    })
    it('Assert newUser last name type is a string', () => {
        assert.equal(typeof(newUser.lastName), 'string');
    })
    it('Assert newUser first name is not undefined', () => {
        assert.notEqual(typeof(newUser.firstName), 'undefined');
    })
    it('Assert newUser last name is not undefined', () => {
        assert.notEqual(typeof(newUser.lastName), 'undefined');
    })
    it('Assert newUser age is at least 18', () => {
        assert.isAtLeast((newUser.age), 18);
    })
    it('Assert newUser age type is a number', () => {
        assert.equal(typeof(newUser.age), 'number');
    })
    it('Assert newUser contact number type is a string', () => {
        assert.equal(typeof(newUser.contactNumber), 'string');
    })
    it('Assert newUser batch number type is a number', () => {
        assert.equal(typeof(newUser.batchNumber), 'number');
    })
    it('Assert newUser batch number is not undefined', () => {
        assert.notEqual(typeof(newUser.batchNumber), 'undefined');
    })
})