const express = require('express')
const app = express()
const port = 5000

app.use(express.json());
app.get('/', (req, res) => res.send('Hello World!'))

const newUser = {
    firstName: 'John',
    lastName: 'Dela Cruz',
    age: 18,
    contactNumber: '09123456789',
    batchNumber: 151,
    email:'john.delacruz@gmail.com',
    password: 'sixteencharacters'
}

app.listen(port, () => console.log(`Server running at PORT ${port}`))

module.exports = {
    newUser: newUser
}